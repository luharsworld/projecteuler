
public class Problem37{
    public static void main(String args[]){
        for (int i = 11; i < 10000000;i++){
            if (isPrime(i) && isTrunk(i)){
                System.out.println(i);

            }
        }
        // System.out.println(isTrunk(23));

    }

    static boolean isPrime (int p){
        boolean factors = false;
        for (int j = 2; j <= p/2;j++){
            if (p % j == 0){
                factors = true;
                break;
            }
        }
        if (p == 1){
            return false;
        }
        if (factors == true){
            return false;
        }
        else{
            return true;
        }
    }

    static boolean isTrunk(int t){
        String s = Integer.toString(t);
        for (int k = 0;k < s.length();k++){
            String str = s.substring(0,s.length() - k);
            int x = Integer.parseInt(str);

            if (!isPrime(x)){
                return false;
            }

        }
        for (int k = 0;k < s.length();k++){
            String str = s.substring(k,s.length());
            int x = Integer.parseInt(str);

            if (!isPrime(x)){
                return false;
            }

        }

        return true;
    }
}
