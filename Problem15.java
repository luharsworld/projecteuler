
/**
 * Write a description of class latticepaths here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Problem15
{
    public static void main (String args[]){
        System.out.println(combinations(40,20));
        
    }
    static long factorial (long x){
       long product = 1;
        for (long i = 2; i <= x; i++){
            product *= i;
        }
        return product;
    }
    static long combinations (long x, long y){
        long a = factorial(x);
        long b = factorial(y);
        long c = factorial(x - y);
        return a / (b*c);
    }
}
