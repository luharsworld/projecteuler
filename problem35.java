import java.lang.String;
/**
 * Write a description of class d here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class problem35
{
    public static void main (String args[]){
        int c = 0;
        System.out.println(prime(97));
        System.out.println(allOdd(98));
        System.out.println(allOdd(99));
        for (int i = 101; i < 1000000;i++){
            if (allOdd(i) && prime(i)){
                if (circle(i)){
                    System.out.println(i);
                    c++;
                }
            }
        }
        System.out.println(c);
    }

    static boolean prime(int n){
        for (int i = 2; i < Math.sqrt(n);i++){
            if (n%i == 0){
                return false;
            }
        }
        return true;
    }

    static boolean allOdd(int n){
        String s = Integer.toString(n);
        for (int i = 0; i < s.length();i++){
            if (Integer.parseInt(s.substring(i,i+1)) %2 == 0){
                return false;
            }
        }
        return true;
    }

    static boolean circle(int n){
        String s = Integer.toString(n);
        if (s.length() == 3){
            String temp = s.substring(1,3) + s.substring(0,1);
            if (!prime(Integer.parseInt(temp))){
                return false;
            }
            temp = s.substring(2,3) + s.substring(0,2);
            if (!prime(Integer.parseInt(temp))){
                return false;
            }
            return true;
        }
        if (s.length() == 4){
            String temp = s.substring(1,4) + s.substring(0,1);
            if (!prime(Integer.parseInt(temp))){
                return false;
            }
            temp = s.substring(2,4) + s.substring(0,2);
            if (!prime(Integer.parseInt(temp))){
                return false;
            }
            temp = s.substring(3,4) + s.substring(0,3);
            if (!prime(Integer.parseInt(temp))){
                return false;
            }
            return true;
        }
        if (s.length() == 5){
            String temp = s.substring(1,5) + s.substring(0,1);
            if (!prime(Integer.parseInt(temp))){
                return false;
            }
            temp = s.substring(2,5) + s.substring(0,2);
            if (!prime(Integer.parseInt(temp))){
                return false;
            }
            temp = s.substring(3,5) + s.substring(0,3);
            if (!prime(Integer.parseInt(temp))){
                return false;
            }
            temp = s.substring(4,5) + s.substring(0,4);
            if (!prime(Integer.parseInt(temp))){
                return false;
            }
            return true;

        }
        if (s.length() == 6){
            String temp = s.substring(1,6) + s.substring(0,1);
            if (!prime(Integer.parseInt(temp))){
                return false;
            }
            temp = s.substring(2,6) + s.substring(0,2);
            if (!prime(Integer.parseInt(temp))){
                return false;
            }
            temp = s.substring(3,6) + s.substring(0,3);
            if (!prime(Integer.parseInt(temp))){
                return false;
            }
            temp = s.substring(4,6) + s.substring(0,4);
            if (!prime(Integer.parseInt(temp))){
                return false;
            }
            temp = s.substring(5,6) + s.substring(0,5);
            if (!prime(Integer.parseInt(temp))){
                return false;
            }
            return true;
        }
        return false;
    }
}
