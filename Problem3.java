
/**
 * Write a description of class LargestPrimeFactor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Problem3
{
    public static void main (String args[]){
        for (int i = 0;i < 20;i++){
            if (isPrime(i*(i+1) + 17) == false ){
                System.out.println(i);
                System.out.println(i*(i+1) + 17);
            }
        }
    }

    static boolean isPrime (long p){
        boolean factors = false;
        for (long j = 2; j <= Math.sqrt(p);j++){
            if (p % j == 0){
                factors = true;
                break;
            }
        }
        if (factors == true){
            return false;
        }
        else{
            return true;
        }
    }
}
